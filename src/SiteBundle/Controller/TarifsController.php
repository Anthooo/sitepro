<?php

namespace SiteBundle\Controller;

use SiteBundle\Entity\Tarifs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tarif controller.
 *
 */
class TarifsController extends Controller
{
    /**
     * Lists all tarif entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tarifs = $em->getRepository('SiteBundle:Tarifs')->findAll();

        return $this->render('@Site/Admin/tarifs/index.html.twig', array(
            'tarifs' => $tarifs,
        ));
    }

    /**
     * Creates a new tarif entity.
     *
     */
    public function newAction(Request $request)
    {
        $tarif = new Tarifs();
        $form = $this->createForm('SiteBundle\Form\TarifsType', $tarif);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tarif);
            $em->flush();

            return $this->redirectToRoute('tarifs_index', array('id' => $tarif->getId()));
        }

        return $this->render('@Site/Admin/tarifs/new.html.twig', array(
            'tarif' => $tarif,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tarif entity.
     *
     */
    public function showAction(Tarifs $tarif)
    {
        $deleteForm = $this->createDeleteForm($tarif);

        return $this->render('@Site/Admin/tarifs/show.html.twig', array(
            'tarif' => $tarif,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tarif entity.
     *
     */
    public function editAction(Request $request, Tarifs $tarif)
    {
        $deleteForm = $this->createDeleteForm($tarif);
        $editForm = $this->createForm('SiteBundle\Form\TarifsType', $tarif);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tarifs_index', array('id' => $tarif->getId()));
        }

        return $this->render('@Site/Admin/tarifs/edit.html.twig', array(
            'tarif' => $tarif,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tarif entity.
     *
     */
    public function deleteAction(Request $request, Tarifs $tarif)
    {
        $form = $this->createDeleteForm($tarif);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tarif);
            $em->flush();
        }

        return $this->redirectToRoute('tarifs_index');
    }

    /**
     * Creates a form to delete a tarif entity.
     *
     * @param Tarifs $tarif The tarif entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Tarifs $tarif)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tarifs_delete', array('id' => $tarif->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
