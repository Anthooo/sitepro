<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SiteBundle\Entity\Portfolio;
use Symfony\Component\BrowserKit\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $services = $em->getRepository('SiteBundle:Services')->findAll();
        $abouts = $em->getRepository('SiteBundle:About')->findAll();
        $skills = $em->getRepository('SiteBundle:Skills')->findAll();
        $portfolios = $em->getRepository('SiteBundle:Portfolio')->findAll();
        $tarifs = $em->getRepository('SiteBundle:Tarifs')->findAll();

        return $this->render('SiteBundle:Default:index.html.twig', array(
            'services' => $services,
            'abouts' => $abouts,
            'skills' => $skills,
            'portfolios' => $portfolios,
            'tarifs' => $tarifs
        ));
    }

    public function adminAction()
    {
        return $this->render('SiteBundle:Admin:admin.html.twig');
    }

    public function portfolioAction(Portfolio $portfolio)
    {
        $em = $this->getDoctrine()->getManager();

        $portfolio = $em->getRepository('SiteBundle:Portfolio')->findOneById($portfolio);

        return $this->render('SiteBundle:Portfolio:portfolio-single.html.twig', array(
            'portfolio' => $portfolio
        ));
    }


    public function sendAction(\Symfony\Component\HttpFoundation\Request $request)
    {
        $from = $this->getParameter('mailer_user');
    // Instanciation des variables name, firstname, mail, sujet, msg pour récupérer la data
        $name = $request->request->get('nom');
        $mail = $request->request->get('mail');
        $sujet = $request->request->get('sujet');
        $msg = $request->request->get('message');
    // Instanciation d'un nouveau message vers l'administrateur avec la prise en compte des variables
        $message = \Swift_Message::newInstance()
            ->setSubject('Contact')
            ->setFrom(array($from => 'Anthony Menet'))
            ->setTo($from)
            ->setBody(
                $this->renderView(
                    '@Site/Default/mail.html.twig',
                    array(
                        'nom' => $name,
                        'mail' => $mail,
                        'sujet' => $sujet,
                        'message' => $msg
                    )
                ),
                'text/html'
            );
        // Instanciation d'un nouveau message vers l'utilisateur avec la prise en compte des variables
        $message2 = \Swift_Message::newInstance()
            ->setSubject('Copie Contact Anthony MENET')
            ->setFrom(array($from => 'Anthony MENET'))
            ->setTo($mail)
            ->setBody(
                $this->renderView(
                    "@Site/Default/mail2.html.twig",
                    array(
                        'nom' => $name,
                        'mail' => $mail,
                        'sujet' => $sujet,
                        'message' => $msg
                    )
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);
        $this->get('mailer')->send($message2);

// Ajout message sur page d'accueil pour informé de l'envoi du mail
        $this->addFlash(
            'notice',
            'Votre message a bien été envoyé'
        );
        return $this->redirectToRoute('site_homepage');

    }

};
