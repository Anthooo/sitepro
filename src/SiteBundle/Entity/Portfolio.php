<?php

namespace SiteBundle\Entity;

/**
 * Portfolio
 */
class Portfolio
{
   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $titre;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $typeClient;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $technoUtilisees;

    /**
     * @var string
     */
    private $url;

    /**
     * @var \SiteBundle\Entity\Image
     */
    private $image;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Portfolio
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Portfolio
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set typeClient
     *
     * @param string $typeClient
     *
     * @return Portfolio
     */
    public function setTypeClient($typeClient)
    {
        $this->typeClient = $typeClient;

        return $this;
    }

    /**
     * Get typeClient
     *
     * @return string
     */
    public function getTypeClient()
    {
        return $this->typeClient;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Portfolio
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set technoUtilisees
     *
     * @param string $technoUtilisees
     *
     * @return Portfolio
     */
    public function setTechnoUtilisees($technoUtilisees)
    {
        $this->technoUtilisees = $technoUtilisees;

        return $this;
    }

    /**
     * Get technoUtilisees
     *
     * @return string
     */
    public function getTechnoUtilisees()
    {
        return $this->technoUtilisees;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Portfolio
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set image
     *
     * @param \SiteBundle\Entity\Image $image
     *
     * @return Portfolio
     */
    public function setImage(\SiteBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \SiteBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }
}
